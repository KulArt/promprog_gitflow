#include <iostream>

class ImportantHello() {
 public:
  ImportantHello(const std::string& name = "Pavel") {
  	std::cout << "Important hello," << name << "!!!\n";
  }
}


int main() {
  ImportantHello();
}