#include <iostream>

class Solver {
 public:
  void Greet(const std::string& name) {
  	if (greetings_cnt == 0) {
  	  std::cout << "Hello, " << name << "!\n";
  	} else {
  	  std::cout << "Hello one more time, " << name << "!\n";
  	}
  	++greetings_cnt;
  }

 private:
  int greetings_cnt = 0;
}

int main() {
  Solver solver;
  solver.Greet("Pavel");
}